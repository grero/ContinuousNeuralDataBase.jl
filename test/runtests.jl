using ContinuousNeuralDataBase
using ContinuousNeuralDataBase: LowpassArgs, HighpassArgs, BandpassArgs, ContinuousData
using DataProcessingHierarchyTools
const DPHT = DataProcessingHierarchyTools
using MAT
using Test
using Random

tdir = tempdir()
channeldir = "Pancake/20130923/session01/array01/channel001"
cd(tdir) do
    mkpath(channeldir)
    cd(channeldir) do
        @testset "Filenames" begin
            largs = LowpassArgs(Float64)
            @test DPHT.filename(largs) == "lowpass.mat"
            hargs = HighpassArgs(Float64)
            @test DPHT.filename(hargs) == "highpass.mat"
        end
        @testset "Basic" begin
            X = fill(0.0, 30_000)
            RNG = MersenneTwister(UInt32(1234))
            X[1] = randn(RNG)
            #brownian motion
            for i in 2:length(X)
                X[i] = X[i] + randn(RNG)
            end
            largs = LowpassArgs(Float64)
            @test largs.sampling_rate == 1000.0
            cdata = ContinuousData(largs, X, 30_000)
            DPHT.save(cdata, "lowpass.mat")
            @test length(cdata.data) == 1000
            @test isfile("lowpass.mat")
            Q = MAT.matread("lowpass.mat")
            cdata2 = convert(typeof(cdata), Q)
            @test cdata2.data ≈ cdata.data
            cdata3 = ContinuousData("lowpass.mat")
            @test cdata3.data == cdata.data
            #TODO: test that the data actually makes sense
            bargs = BandpassArgs(Float64, 40.0, 60.0, 1000.0)
            fname = DPHT.filename(bargs)
            bdata = ContinuousData(bargs)
            @test isfile(fname)
            @test length(bdata.data) == length(cdata.data)
        end
    end
end
